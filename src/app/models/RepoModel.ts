/** RepoMVC model definitions **/

export interface IRepoModel {
  id: number;
  name: string;
  full_name: string;
  owner: any;
  html_url: string;
  private: boolean;
  description: string;
  url: string;
  created_at: Date;
  stargazers_count: number;
  watchers_count: number;
  forks_count: number;
}

export class RepoModel implements IRepoModel {
  public id: number;
  public name: string;
  public full_name: string;
  public owner: any;
  public html_url: string;
  public private: boolean;
  public description: string;
  public url: string;
  public created_at: Date;
  public stargazers_count: number;
  public watchers_count: number;
  public forks_count: number;

  constructor(data?: IRepoModel) {
    if (data) {
      Object.assign(this, data);
    }
  }
}