export interface Fetchable<T> {
    isLoading: boolean;
    data: T;
    error?: any;
}