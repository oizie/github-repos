import * as React from 'react';
import * as style from './style.css';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from 'redux';
import { RouteComponentProps } from 'react-router';
import { RepoActions, SearchActions } from 'app/actions';
import { RootState } from 'app/reducers';
import { omit } from 'app/utils';
import { Header, RepoList, Footer, TextInput } from 'app/components';

export namespace App {
  export interface Props extends RouteComponentProps<void> {
    repos: RootState.ReposState;
    search: RootState.SearchState;
    repoActions: RepoActions;
    searchActions: SearchActions;
  }
}

@connect(
  (state: RootState): Pick<App.Props, 'repos' | 'search'> => {
    // const hash = state.router.location && state.router.location.hash.replace('#', '');
    return { repos: state.repos, search: state.search };
  },
  (dispatch: Dispatch): Pick<App.Props, 'repoActions' | 'searchActions'> => ({
    repoActions: bindActionCreators(omit(RepoActions, 'Type'), dispatch),
    searchActions: bindActionCreators(omit(SearchActions, 'Type'), dispatch)
  })
)
export class App extends React.Component<App.Props> {
  static defaultProps: Partial<App.Props> = {
  };

  constructor(props: App.Props, context?: any) {
    super(props, context);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  handlePageChange(page: number): void {
    this.props.repoActions.searchRepo(this.props.search.currentText, page);
  }

  handleSearch(text: string): void {
    this.props.searchActions.changeCurrentText({ currentText: text });
    this.props.repoActions.searchRepo(text, 1);
  }

  renderList(actions: RepoActions, fetchableReposData: RootState.ReposState) {
    if (fetchableReposData.isLoading) {
      return <div className={style.loading}>Loading...</div>;
    }
    if (fetchableReposData.error) {
      return <div className={style.error}>{ fetchableReposData.error }</div>
    }
    return <RepoList actions={actions} onPageChange={(page) => this.handlePageChange(page)} repos={fetchableReposData.data} />;
  }

  render() {
    const { repos, repoActions } = this.props;
    return (
      <div className={style.normal}>
        <Header />
        <TextInput onChange={(text) => this.handleSearch(text)} />
        { this.renderList(repoActions, repos) }
        <Footer />
      </div>
    );
  }
}
