import axios, { AxiosInstance, CancelTokenStatic, CancelTokenSource } from 'axios';
import { Pageable, IRepoModel, RepoModel } from '../models';

export class RepoService {

    private _axios: AxiosInstance;
    private _baseUrl: string;
    private _cancelToken: CancelTokenStatic;
    private _cancelSource: CancelTokenSource;
    public pageSize: number = 10;

    constructor(axiosInst: AxiosInstance, baseUrl: string) {
        this._axios = axiosInst;
        this._baseUrl = baseUrl;
        this._cancelToken = axios.CancelToken;
    }

    public getList(text: string, page: number): Promise<Pageable<IRepoModel[]>> {
        if (this._cancelSource) {
            this._cancelSource.cancel('Operation canceled by other request.');
        }
        this._cancelSource = this._cancelToken.source();
        return this._axios.get(`${this._baseUrl}?q=${text}in:name&page=${page}&per_page=${this.pageSize}`, {
            cancelToken: this._cancelSource.token
        })
        .then(response => {
            if (response && response.data && response.data.total_count > 0) {
                let data: IRepoModel[] = response.data.items.map((x: IRepoModel) => new RepoModel(x));
                return {
                    data,
                    page,
                    total: response.data.total_count
                };
            }
            if (response.data && response.data.total_count === 0) {
                throw new Error("There is no repository with that name!");
            }
            throw new Error("There was an error in the call");
            return {
                data: [],
                page: 1,
                total: 0
            };
        });
    }
}