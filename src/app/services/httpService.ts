import axios, { AxiosInstance } from 'axios';
import * as config from '../config/api';
import { RepoService } from 'app/services/repoService';

export default class HttpService {

    private static _instance:HttpService = new HttpService();
    private _axios: AxiosInstance;

    constructor() {
        if(HttpService._instance){
            throw new Error("Error: Instantiation failed: Use HttpService.getInstance() instead of new.");
        }
        HttpService._instance = this;
        this._axios = axios.create({
            headers: {
                "Accept": "application/vnd.github.v3+json",
                "Authorization": "token d7b09bac653f6ca82c098930a39e10e319e2b990"
            }
        });
        // Would be able to create interceptors here to be able to process the data as needed
        this.repoService = new RepoService(this._axios, `${config.getApiConfig()}repositories`);
    }

    public static getInstance(): HttpService
    {
        return HttpService._instance;
    }

    public repoService: RepoService;
}