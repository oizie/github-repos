import { createAction } from 'redux-actions';
import { Search } from '../models';

export namespace SearchActions {
  export enum Type {
    CHANGE_CURRENT_TEXT = 'CHANGE_CURRENT_TEXT',
  };

  export const changeCurrentText = createAction<PartialPick<Search, 'currentText'>>(Type.CHANGE_CURRENT_TEXT);
}

export type SearchActions = Omit<typeof SearchActions, 'Type'>;