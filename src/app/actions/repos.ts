import { createAction } from 'redux-actions';
import { IRepoModel, Pageable } from 'app/models';
import { isCancelationError } from 'app/utils';
import HttpService from 'app/services/httpService';

export namespace RepoActions {
  export enum Type {
    SEARCH_REPOS_BEGIN = 'SEARCH_REPOS_BEGIN',
    SEARCH_REPOS_ERROR = 'SEARCH_REPOS_ERROR',
    SEARCH_REPOS_SUCCESS = 'SEARCH_REPOS_SUCCESS'
  }

  export const searchRepoBegin = createAction(Type.SEARCH_REPOS_BEGIN);
  export const searchRepoError = createAction<string>(Type.SEARCH_REPOS_ERROR);
  export const searchRepoSuccess = createAction<Pageable<IRepoModel[]>>(Type.SEARCH_REPOS_SUCCESS);
  export const searchRepo = (text: string, page: number) => (dispatch: Function) => {
    if (!text) {
      const emptyResult: Pageable<IRepoModel[]> = {
        data: [],
        page: 1,
        total: 0
      };
      dispatch(searchRepoSuccess(emptyResult));
      return new Promise(resolve => resolve());
    } else {
      dispatch(searchRepoBegin());
      return HttpService.getInstance().repoService.getList(text, page)
      .then((result) => {
        dispatch(searchRepoSuccess(result));
      })
      .catch(error => {
        if (!isCancelationError(error)) {
          dispatch(searchRepoError(error));
        }
      });
    }
  };
}

export type RepoActions = Omit<typeof RepoActions, 'Type'>;
