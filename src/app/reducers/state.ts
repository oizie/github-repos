import { IRepoModel, Fetchable, Pageable, Search } from 'app/models';
import { RouterState } from 'react-router-redux';

export interface RootState {
  repos: RootState.ReposState;
  search: RootState.SearchState;
  router: RouterState;
}

export namespace RootState {
  export type ReposState = Fetchable<Pageable<IRepoModel[]>>;
  export type SearchState = Search;
}
