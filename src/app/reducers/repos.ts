import { handleActions } from 'redux-actions';
import { RootState } from 'app/reducers/state';
import { RepoActions } from 'app/actions/repos';
import { IRepoModel, Pageable } from 'app/models';

const initialState: RootState.ReposState = {
  isLoading: false,
  data: {
    page: 1,
    data: [],
    total: 0
  }
};

export const repoReducer = handleActions<RootState.ReposState, Pageable<IRepoModel[]>>(
  {
    [RepoActions.Type.SEARCH_REPOS_BEGIN]: (state) => {
      return {
        ...state,
        isLoading: true,
        error: null
      };
    },
    [RepoActions.Type.SEARCH_REPOS_SUCCESS]: (state, action) => {
      if (action && action.payload) {
        return {
          isLoading: false,
          error: null,
          data: action.payload
        };
      } else {
        return state;
      }
    },
    [RepoActions.Type.SEARCH_REPOS_ERROR]: (state, action: any) => {
      return {
        ...state,
        isLoading: false,
        error: action.payload.message
      };
    }
  },
  initialState
);
