import { handleActions } from 'redux-actions';
import { RootState } from 'app/reducers/state';
import { SearchActions } from 'app/actions/search';
import { Search } from 'app/models';

const initialState: RootState.SearchState = {
  currentText: ""
};

export const searchReducer = handleActions<RootState.SearchState, Search>(
  {
    [SearchActions.Type.CHANGE_CURRENT_TEXT]: (state, action) => {
      if (action && action.payload) {
        return {
          ...state,
          currentText: action.payload.currentText
        };
      } else {
        return state;
      }
    }
  },
  initialState
);
