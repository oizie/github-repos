function getDefaultUrl(): string {
    return "https://api.github.com/search/";
}

function getApiConfig(): string {
    let url: string = "#{API_URL}";

    return  !url.includes("{API_URL}") ? url : getDefaultUrl();
}

export { getApiConfig };