import * as React from 'react';
import * as style from './style.css';

export namespace Footer {
  export interface Props {
  }
}

export class Footer extends React.Component<Footer.Props> {
  static defaultProps: Partial<Footer.Props> = {
  };
  render() {
    return (
      <footer className={style.normal}>
      </footer>
    );
  }
}
