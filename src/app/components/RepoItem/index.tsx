import * as React from 'react';
import * as style from './style.css';
import { RepoModel } from 'app/models';

export namespace RepoItem {
  export interface Props {
    repo: RepoModel;
  }
}

export class RepoItem extends React.Component<RepoItem.Props> {
  constructor(props: RepoItem.Props, context?: any) {
    super(props, context);
    this.state = { editing: false };
  }

  render() {
    const { repo } = this.props;
    return <li className={style.normal}>
      <label className={style.name}>{ repo.name }</label>
      <div className={style.info}>
        <span><label>Stars:</label>{ repo.stargazers_count }</span>
        <span><label>Watchers:</label>{ repo.watchers_count }</span>
        <span><label>Forks:</label>{ repo.forks_count }</span>
      </div>
    </li>;
  }
}
