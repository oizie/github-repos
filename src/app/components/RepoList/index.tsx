import * as React from 'react';
import * as style from './style.css';
import { RepoActions } from 'app/actions/repos';
import { RepoItem } from '../RepoItem';
import { IRepoModel, Pageable } from 'app/models';
import Pagination from "react-js-pagination";

export namespace RepoList {
  export interface Props {
    repos: Pageable<IRepoModel[]>;
    actions: RepoActions;
    onPageChange: (page: number) => void;
  }
}

export class RepoList extends React.Component<RepoList.Props> {
  handlePageChange(pageNumber: number) {
    this.props.onPageChange(pageNumber);
  }

  render() {
    const { repos } = this.props;

    if (repos.total === 0) {
      return null;
    }
    return (
      <section className={style.main}>
        <ul className={style.normal}>
          {
            repos.data.map((repo) => (
            <RepoItem
              key={repo.id}
              repo={repo}
            />
          ))}
        </ul>
        <div className={style.pagination}>
          <Pagination
            activePage={repos.page}
            itemsCountPerPage={10}
            totalItemsCount={repos.total}
            pageRangeDisplayed={5}
            hideFirstLastPages={true}
            onChange={(pageNumber: number) => this.handlePageChange(pageNumber)}
          />
        </div>
      </section>
    );
  }
}
