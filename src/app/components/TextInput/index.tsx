import * as React from 'react';
import * as style from './style.css';

export namespace TextInput {
  export interface Props {
    text?: string;
    placeholder?: string;
    onChange: (text: string) => void;
  }

  export interface State {
  }
}

export class TextInput extends React.Component<TextInput.Props, TextInput.State> {
  constructor(props: TextInput.Props, context?: any) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    const text = event.target.value.trim();
    this.props.onChange(text);
  }

  render() {
    return (
      <input
        className={style.new}
        type="text"
        autoFocus
        placeholder={this.props.placeholder}
        value={this.props.text}
        onChange={this.handleChange}
      />
    );
  }
}
